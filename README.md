This scripts takes an XML (TEI) database with original spellings and a tab-separated list of character alternation pairs, and generates a new XML document with standardised variants inserted.
It runs over an entire directory, processing all files ending with .xml.
The -v option also gives a two-column output: original spelling <tab> possible variant.

The latest version of the script is daisyBatesNormaliser.py.
The slow version creates all possible combinations of variants but is too slow to be practical.